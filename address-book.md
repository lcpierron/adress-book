# Projet: adress-book

Auteurs: Michael Kölling and David J. Barnes
Traduction: Laurent Pierron

Ce projet fait partie du matériel pour le livre

   **Objects First with Java - A Practical Introduction using BlueJ
   Fifth edition
   David J. Barnes and Michael Kölling
   Pearson Education, 2012**

Il est expliqué dans le chapitre 12.

Une réalisation d'un carnet d'adresses avec une interface graphique
optionnelle. La recherche est flexible : les enregistrements
peuvent être retrouvés par une définition partielle du nom ou
du numéro de téléphone. Ce projet utilise intensivement les exceptions.

- - -

## Préparation du projet

1. Créer une bifurcation (fork) du projet **adress-book** dans votre compte Bitbucket.
2. Ajouter votre binôme et `lcpierron` en administrateur du projet.
3. Cloner votre projet sur votre machine locale.

- - -

## Expression du besoin

Vous devez réaliser un gestionnaire de carnet d'adresses, qui devra
s'intégrer dans divers systèmes nécessitant un accès à
un carnet d'adresses (smartphone, internet, application desktop texte et graphique, tablette, etc.)
et fonctionner en serveur pour d'autres applications
(messagerie, numérotation téléphonique, rendez-vous, etc.).

Une entrée de carnet d'adresses sera composée des informations suivantes :
nom, numéro de téléphone, adresse

Les fonctionnalités du carnet d'adresses seront les suivantes :

* Ajout d'un contact
* Suppression d'un contact
* Changement des informations d'un contact
* Recherche de contacts par le nom ou le numéro de téléphone partiel
* Liste complète de tous les contacts triés

## Définition des classes

L'analyse de l'expression des besoins, nous invite à proposer
deux classes pour représenter le domaine : `AddressBook` et `ContactDetails`.

> Est-ce que vous pouvez imaginer quelles autres classes il pourrait y
> avoir pour un carnet d'adresses complet ?

Les contacts devront être triés, il faudra donc
définir une relation d'ordre sur les contacts afin d'être
utilisé par une méthode de tri de Java. Pour que des objets
mis dans des collections puissent être triés ils doivent instancier l'interface :
`Comparable<T>` (http://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html)

![Diagramme de classes UML](address-book.png)

La collection d'adresses est représentée sous la forme d'un tableau
associatif en l'occurrence `TreeMap<String, ContactDetails>`. `TreeMap` a la
particularité de garder les entrées triées, pour cela il faut
que les clès instancient l'interface `Comparable`, c'est le cas pour
la classe `String`.

## Première version textuelle

Afin de pouvoir tester votre classe, une première version textuelle est
réalisée. Elle s'inspire du programme `tech-support`, que nous avons déjà
vu. En plus des deux classes pour le carnet d'adresses, elle ajoute :

* AddressBookDemo : un exemple de carnet avec des données pour tester
les classes `AddressBook` et `ContactDetails`.
* AddressBookTextInterface : un *contrôleur* et la *vue* pour une interaction
textuelle avec le *modèle* de carnet d'adresses.
* Parser : une classe pour lire des commandes dans la console comme dans `tech-support`.
* CommandWords : les commandes pour agir sur le carnet d'adresses

1. Accès à la version 1 : `git checkout -b study v1-txt`

Si on se focalise sur la classe `AddressBook`, c'est ce qui nous intéresse
au niveau du développement, nous constatons que cette dernière
a plusieurs problèmes liés au fait que l'objet `ContactDetails`
peut avoir la valeur `null` dans certaines instructions.

> Mettre en évidence les instructions ou la valeur `null` peut
> apparaître. Que devient le programme ?

En Java par défaut tout objet a la valeur `null`, quand une méthode
est utilisée sur cet objet `null`, Java envoie une exception
`NullPointerException` et arrête l'exécution du programme
sauf si le programmeur a prévu d'attrapper l'exception.
Malgré l'information de la ligne où a eu lieu cette erreur,
il n'est pas toujours facile de savoir pourquoi l'objet avait
la valeur `null`, l'utilisation d'un bon débogueur comme on l'a vu peut
s'avérer utile.

> Dans d'autres langages de programmation, notamment C, les variables ne sont
> pas toujours initialisées à une valeur par défaut et à l'exécution
> vous n'avez pas d'informations sur l'endroit où s'est révélée l'erreur,
> elle peut même passer complètement inaperçue dans ce cas le programme
> n'aura sûrement pas le comportenemnt attendu.
> En C vous aurez en général le message d'erreur : `Segmentation fault`
> (http://en.wikipedia.org/wiki/Segmentation_fault)


## Seconde version textuelle : contrôle des entrées par test

Afin d'éviter les erreurs précédentes des tests pour contrôler
les paramètres ont été ajoutés dans les méthodes suivantes
de la classe `AddressBook` : `addDetails`, `changeDetails`,
`search` et `removeDetails`.

Pour passer à la version 2, dans la branch `study` :

1. `git merge v2-txt`

On trouvera fréquemment l'instruction suivante :

```java
if (object != null) {
  ... RESTE DU PROGRAMME ...
}
```

On peut lui préférer pour des raisons de lisibilité :

```java
if (object == null) { return; }

... RESTE DU PROGRAMME ...
```

> Mais que doit renvoyer la méthode si on attend une valeur ?

Vous noterez que dans cette version, il n'y a pas de messages
d'erreur envoyés à l'utilisateur. Faudrait-il des messages
d'erreur pour l'utilisateur ? Que devrait-il contenir ?
Quelles sont les alternatives possibles pour garder une
trace des erreurs ? Connaissez-vous des logiciels qui utilisent
des méthodes alternatives pour tracer les erreurs ?

## Troisième version textuelle : utilisation des exceptions

L'utilisation de tests pour contrôler les entrées d'une méthode
peut être utilisée, mais il est des situations où l'on
ne  peut pas se contenter d'ignorer une erreur en silence,
on pourrait ajouter des codes de retour aux méthodes
comme un `boolean` pour indiquer que l'opération s'est bien passée.
Ce type de technique est utilisé en C, qui n'a pas de mécanismes
d'exception, en Java on utilisera une exception pour notifier
une erreur au programme appelant la méthode.

Le principe des exceptions est assez simple, il consiste à envoyer
un objet de type `Throwable` au programme appelant avec la méthode `throw`,
ainsi :

```java
/**
* ...
* @throws IllegalArgumentException if the key is invalid.
*/
public ContactDetails getDetails(String key) {
  if (key == null) {
    throw new IllegalArgumentException(
        "null key in getDetails")
  }
  return book.get(key);
}
```

On précisera également dans la Javadoc avec la balise `@throws` l'émission
d'exception afin que l'utilisateur du programme puisse traiter l'exception.

Il existe deux classes mères pour les exceptions en Java, qui
héritent de `Throwable` :

* `Error` : pour les erreurs critiques de la machine virtuelle Java,
qui ne devraient pas être rattrappées,
il n'est pas nécessaire de les déclarer dans les méhodes.
* `Exception` : les erreurs qu'il est possible de traiter
à l'intérieur des programmes.

La classe `Exception` (http://docs.oracle.com/javase/8/docs/api/java/lang/Exception.html) est divisée également en deux catégories :

* *unchecked* : les sous-classes de la classe `RuntimeException`
* *checked* : les sous-classes de la classe `Exception`, qui ne sont pas
sous-classe de `RunTimeException`.

Les méthodes qui utilisent des exceptions *checked* comme celles
qui se trouvent dans le package *java.io* doivent avoir un entête
contenant le mot-clé `throws` comme ici :

```java
public void saveToFile(String destinationFile)
    throws IOException
{
    ...
}
```

### Récupération d'une exception

Le programme appelant une méthode pouvant envoyer une exception
peut la récupérer en ayant encapsuler l'appel de la méthode
dans un bloc `try .. catch ..` comme ici :

```java
try {
  // Protège une ou plusieurs instructions
} catch (Exception e) {
  // Trace et récupère l'exception.
}
```

Supposons que nous ayons une méthode `saveToFile` pour notre carnet
d'adresses, cette méthode peut envoyer une exception au cas où
elle ne pourrait pas enregistrer notre carnet d'adresses, car le disque
est plein ou interdit en écriture. Il est bien entendu essentiel
de notifier ce problème à l'utilisateur, mais il serait dommage
d'arrêter le programme pour cela, car il est peut-être possible d'enregistrer
ailleurs. Voici un exemple d'appel protégé à `saveFile` :

```java
String filename = null;
try {
  filename = request-a-file-from-the-user;
  addressbook.saveToFile(filename);
  successful = true;
} catch (IOException e) {
  System.out.println("Impossible d'enregistrer " + filename);
  successful = false;
}
```

### Etude de la version 3 du carnet d'adresses

Les cas d'objet `null` envoient des exceptions : `IllegalArgumentException`,
une exception plus précise aurait pu être utilisée, trouvez cette exception dans
la documentation Java.

Dans `ContacDetails` a été utilisé l'exception : `IllegalStateException`,
est-ce judicieux ?

### Ajout d'une exception personnalisée

En Java il est possible d'ajouter ses propres exceptions, dans
le programme a été ajouté la classe : `NoMatchingDetailsException`,
qui doit être envoyée si aucun contact n'existe pour une clé donnée.

1. Trouver où utiliser cette classe dans `AdressBook`
1. Modifier les méthodes trouvées pour lancer l'exception
1. Récupérer l'exception dans le programme appelant.

## Quatrième version textuelle : utilisation d'un fichier

Un domaine important de la programmation, où la récupération
des erreurs ne peut être ignorée, est celui des entrées-sorties.

Dès que l'on accède à une ressource externe au programme que ce soit
sur un disque local, un fichier sur le réseau, voire un autre
programme sur la même machine, il peut y avoir un problème
qui doit être traité.

Dans ce programme nous allons ajouter la possibilité
d'enregistrer et de relire le carnet d'adresses en format textuel et en format binaire
(*serialisation*) sur un disque local. En Java les classes utilisées
pour manipuler des fichiers textes sont appelées *readers* et *writers*,
pour les fichiers binaires ce sont des *streams*.

Les classes utiles pour effectuer des entrées-sorties sont dans le package :
`java.io` (http://docs.oracle.com/javase/8/docs/api/java/io/package-summary.html)

### La classe `File`

Documentation : http://docs.oracle.com/javase/8/docs/api/java/io/File.html

La classe `File` vous permet de manipuler les noms de fichiers
de manière indépendante du système d'exploitation sous-jacent.

La classe `File` vous permet également de vérifier et modifier les propriétés
d'un fichier comme : son type, ses autorisations d'accès.

La classe `File` est aussi utile pour obtenir le contenu d'un répertoire
et créer des fichiers temporaires.

Un exemple qui affiche tous les fichiers `.java` modifiés dans les
dernières 24 heures :

```java
// Temps préssent en millisecondes
long now = System.currentTimeMillis();

// Durée d'un jour en millisecondes
long unJour = 24 * 3600 * 1000;

// répertoire de recherche
String rep =  "D:/workspace/td-04/src/org/paumard/file";

// construction d'un fichier sur ce répertoire
File repFile =  new File(rep) ;

// on parcourt la liste des fichiers
for (File file : repFile.listFiles()) {
   // on récupère le nom du fichier...
  String fileName = file.getName() ;
  // on récupère la date de modification
  long lastModified = file.lastModified();

  // on vérifie que le fichier a les critères souhaités
  if (file.isFile()                      // vérifie que c'est un fichier
      && fileName.endsWith(".java")      // sélectionne sur le suffixe
      && ((now - lastModified) < unJour) // sélectionne sur la date
    ) {
      System.out.println(fileName);
  }
}
```

### Ecriture dans un fichier texte

Classe `FileWriter` : http://docs.oracle.com/javase/8/docs/api/java/io/FileWriter.html

Java propose de nombreuses classes pour manipuler les entrées-sorties, dans
ce cours nous nous présentrons certaines qui nous semblent les
plus appropriées pour les projets exposés.

Pour écrire dans un fichier texte, la classe la plus simple
est `FileWriter`, qui hérite de la classe `Writer` une méthode `write(String s)` pour écrire
des objets `String`.

Il y a trois étapes pour écrire des données dans un fichier :

1. Ouverture du fichier.
1. Ecriture dans le fichier.
1. Fermeture du fichier.

Chacune de ces étapes peut rencontrer un problème,
donc si on veut s'assurer qu'une erreur d'écriture
n'arrête pas le programme inopinément,
il faut utiliser la structure de base suivante :

```java
FileWriter writer;
try {
  writer = new FileWriter("... nom du fichier ...");
  while (il y a encore du texte à écrire) {
    ...
    writer.write(morceau de texte à écrire);
    ...
  }
  writer.close();
} catch (IOException e) {
  System.out.println("Error encountered writing the file");
} finally {
  if(writer != null) {
    try {
      writer.close();
    } catch(IOException e) {
          System.out.println("Error on closing");
    }
  }
}
```

Voici un exemple de programme protégeant les instructions
d'entrée-sortie pour le carnet d'adresses :

```java
public void saveSearchResults(String keyPrefix) throws IOException
{
  FileWriter writer;
  try {
    File resultsFile = makeAbsoluteFilename(RESULTS_FILE);
    ContactDetails[] results = book.search(keyPrefix);
    writer = new FileWriter(resultsFile);
    for(ContactDetails details : results) {
        writer.write(details.toString());
        writer.write('\n');
        writer.write('\n');
    }
    writer.close();
  } catch(IOException e) {
            System.out.println("Error encountered writing the file: " +
                               RESULTS_FILE);
  } finally {
      if(writer != null) {
            // Catch any exception, but nothing can be done
            // about it.
            try {
                  writer.close();
            } catch(IOException e) {
                  System.out.println("Error on closing: " +
                                       RESULTS_FILE);
            }
      }
  }
}
```


### Lecture d'un fichier texte

Classe `FileReader` : http://docs.oracle.com/javase/8/docs/api/java/io/FileReader.html

Classe `BufferedReader` : http://docs.oracle.com/javase/8/docs/api/java/io/BufferedReader.html

Pour la lecture de texte c'est un peu plus compliqué, il existe bien
une classe `FileReader` qui est l'opposée de `FileWriter`, mais cette
classe a juste une méthode `read`, qui lit un caractère à la fois.
En général on souhaite lire une ligne de texte sous forme de `String`
pour des raisons pratiques et aussi d'efficacité (voir votre cours sur les
  systèmes d'exploitation).
Pour lire une ligne entière il faut utilise un flux *bufferisé*, la classe
BufferedReader est idéale elle dispose des méthodes : `readLine()` pour lire
une ligne et `lines()` qui permet d'effectuer des opérations sur toutes les lignes.

Au vu de ces remarques, voici comment mettre en oeuvre la lecture de fichier :

```java
BufferedReader reader = null;
try {
  reader = new BufferedReader(
                    new FileReader("... nom du fichier ou objet File ..."));
  String line = reader.readLine();
  while (line != null) {
    // faire quelque chose avec line

    // lire la ligne suivante
    line = reader.readLine();
  }
  reader.close();
} catch (FileNotFoundException e) {
  // le fichier n'a pas été trouvé

} catch (IOException e) {
  // quelque chose s'est mal passé
} finally {
  if(reader != null) {
    // le fichier n'a pas été fermé
    try {
      reader.close();
    } catch(IOException e) {
          // Le fichier n'a pas pu être fermé
    }
  }
}
```

### Sérialisation d'objet

Si on souhaite enregistrer des objets dans un fichier texte,
il faut les convertir en objet `String`, pas très compliqué
si on a une méthode `toString`, mais pour les relire
il faut écrire un programme qui interprète le texte lu
pour le transformer en objet, ça se complique !

Une méthode classique pour enregistrer des objets dans
un fichier consiste à les *sérialiser*.
Pour qu'un objet soit *sérialisable*, il doit implémenter
l'interface `Serializable` (http://docs.oracle.com/javase/8/docs/api/java/io/Serializable.html).
Il n'y a aucune méthode à écrire dans la classe utilisant `Serializable`,
java se débrouille pour sérialiser l'objet.

L'écriture d'objet *sérialisé* se fait dans un fichier binaire
et doit donc utiliser un objet de la classe `Stream`.

Exemple d'écriture du carnet d'adresses dans un fichier binaire :

```java
// Ouverture du fichier en écriture
ObjectOutputStream os = new ObjectOutputStream(
                            new FileOutpuStream("moncarnet.dat"));
// Ecriture du carnet d'adresses : book
os.writeObject(book);
// Fermeture du fichier
os.close();
```

On ne peut pas faire plus simple si on omet la gestion des exceptions.

La lecture du carnet d'adresses est aussi simple :
```java
// Ouverture du fichier en lecture
ObjectInputStream is = new ObjectInputStream(
                            new FileInputStream("moncarnet.dat"));
// Lecture du carnet d'adresses : book
AddressBook savedBook = (AddressBook) is.readObject();
// fermeture du fichier
is.close();
```

Vous noterez ici l'utilisation de la conversion de l'objet lu avec
un *cast*, on ne peut pas y échapper car `readObject()` ne peut connaître
le type de l'objet lu.

Documentation `ObjectInputStream` : http://docs.oracle.com/javase/8/docs/api/java/io/ObjectInputStream.html

### La classe `AddressBookFileHandler`

L'enregistrement et la lecture du carnet d'adresses sont
placés dans une classe séparée de la classe `AdressBook`
suivant le principe de division des responsabilités.


## Cinquième version textuelle : utilisation de JSON

Le monde serait parfait si on pouvait combiner la simplicité
d'écriture d'objets *sérialisés* et la souplesse des fichiers
textes pour l'échange de données. C'est ce que propose
de réaliser le format **JSON** (http://www.json.org).

JSON est un format d'échange de données léger. Il est facile à lire ét écrire
par des humains. Il est facile à lire et écrire par des programmes.
Un fichier JSON est construit uniquement sur deux structures :

* Une collection de paires nom/valeur, c'est un tableau associatif (`Map`)
en Java.
* Une liste ordonnée de valeurs, en Java c'est un `array` ou une liste (`List`).

Les valeurs peuvent être des chaînes de caractères, des nombres, des collections, des listes,
`true`, `false` ou `null`.

Les noms dans les collections sont des chaînes de caractères.

Exemple de fichier JSON :
```json
{
 "fruits": [
   { "kiwis": 3,
     "mangues": 4,
     "pommes": null
   },
   { "panier": true },
 ],
 "legumes":
   { "patates": "amandine",
     "figues": "de barbarie",
     "poireaux": false
   }
}
```


Exemple d'enregistrement du carnet d'adresses dans un fichier JSON,
avec l'API `json-simple-1.1.1` (https://code.google.com/p/json-simple/),
qui doit être ajoutée au `CLASSPATH` :

```java
// import org.json.simple.*;
public int saveToJSON(String destinationFile) throws IOException
{
    File destFile = makeAbsoluteFilename(destinationFile);
    ContactDetails[] results = book.search("");
    FileWriter writer = new FileWriter(destFile);
    writer.write("[\n");
    boolean firstDetail = true;
    for(ContactDetails details : results) {
        if (!firstDetail) {
            writer.write(",\n");
        };
        // Create JSON object
        JSONObject obj=new JSONObject();
        obj.put("name", details.getName());
        obj.put("phone", details.getPhone());
        obj.put("address", details.getAddress());
        // Transformation de l'objet JSON en String
        StringWriter out = new StringWriter();
        obj.writeJSONString(out);
        // Write JSON object
        writer.write(out.toString());
    }
    writer.write("\n]\n");
    writer.close();
}
```



Pour en savoir plus :

http://www.oracle.com/technetwork/articles/java/json-1973242.html



## Modification de la version graphique (GUI)

Une variante de la première version du carnet d'adresses utilise
un GUI. Votre travail est de reporter les modifications de la version
textuelle. Une astuce consiste à utiliser à bon escient
les fonctionnalités de `git`, notamment `git merge` ou `git checkout`
pour intégrer les modifications effectuées dans la version
textuelle dans la version graphique. Il est conseillé de se reporter
au cours que vous avez eu au sujet de `git` ou de demander de l'aide
à votre enseignant.

### TÂCHES à effectuer en TP

1. Accédez à la version graphique de l'application : `git checkout -b mygui v1-gui`
1. Ajoutez les exceptions à cette version graphique, vous pouvez
récupérer par une commande `git checkout` le fichier `AddressBook.java` de la version `v3-txt`.
1. Modifiez l'interface graphique pour ajouter la suppression de contacts.
1. *Committez* après avoir testé le bon fonctionnement bien entendu.
1. Ajoutez un *tag* : `git tag v2-gui`
1. Ajoutez la lecture et l'enregistrement de fichier texte et binaire.
1. Modifiez l'interface graphique pour choisir un fichier à lire et écrire.
1. *Committez* après avoir testé le bon fonctionnement bien entendu.
1. Ajoutez un *tag* : `git tag v3-gui`
1. Ajoutez l'utilisation d'un fichier JSON à cette version graphique
1. *Committez* après avoir testé le bon fonctionnement bien entendu.
1. Ajoutez un *tag* : `git tag v4-gui`
1. Synchronisez votre travail avec le serveur Bitbucket par les commandes :

```bash
git push --all  # Synchronise les modifications des branches
git push --tags # Synchronise les *tags*
```

- - -